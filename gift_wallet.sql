-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2017 at 03:05 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gift_wallet`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(100) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `first_name`, `last_name`, `email`, `mobile_number`, `address`, `city`, `country`, `zipcode`, `password`, `created_date`, `modified_date`) VALUES
(1, 'saravana', 'kumar', 'admin@admin.com', '+91 9677421804', 'Shenll Technology Solutions Pvt. Ltd.\r\nPlot No.1069, Munusamy Salai,\r\nK.K Nagar', 'Chennai', 'India', '600078', '123456', '2017-10-09 00:00:00', '2017-10-09 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_charity`
--

CREATE TABLE `tbl_charity` (
  `charity_id` int(11) NOT NULL,
  `charity_name` varchar(150) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(100) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gift_invitation`
--

CREATE TABLE `tbl_gift_invitation` (
  `invite_id` int(11) NOT NULL,
  `invite_name` varchar(150) NOT NULL,
  `invite_address` text NOT NULL,
  `invite_reg_date` datetime NOT NULL,
  `admin_id` int(11) NOT NULL,
  `invite_user_id` int(11) NOT NULL,
  `code` varchar(250) NOT NULL,
  `message` text NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invitation_request`
--

CREATE TABLE `tbl_invitation_request` (
  `invite_request_id` int(11) NOT NULL,
  `invite_id` int(11) NOT NULL,
  `requested_user_id` int(11) NOT NULL,
  `request_date` datetime NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_merchants`
--

CREATE TABLE `tbl_merchants` (
  `merchant_id` int(11) NOT NULL,
  `merchant_full_name` varchar(50) NOT NULL,
  `outlet_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(100) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transactions_receive`
--

CREATE TABLE `tbl_transactions_receive` (
  `transaction_id` int(11) NOT NULL,
  `invite_id` int(11) NOT NULL,
  `invite_request_id` int(11) NOT NULL,
  `paying_user_id` int(11) NOT NULL,
  `receive_user_id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `amount` decimal(16,2) NOT NULL,
  `payment_status` varchar(50) NOT NULL,
  `pay_date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transactions_spent`
--

CREATE TABLE `tbl_transactions_spent` (
  `transaction_id` int(11) NOT NULL,
  `transaction_type` enum('0','1') NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `charity_id` int(11) NOT NULL,
  `amount` decimal(16,2) NOT NULL,
  `spent_user_id` int(11) NOT NULL,
  `payment_status` varchar(50) NOT NULL,
  `spent_date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(100) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_charity`
--
ALTER TABLE `tbl_charity`
  ADD PRIMARY KEY (`charity_id`);

--
-- Indexes for table `tbl_gift_invitation`
--
ALTER TABLE `tbl_gift_invitation`
  ADD PRIMARY KEY (`invite_id`);

--
-- Indexes for table `tbl_invitation_request`
--
ALTER TABLE `tbl_invitation_request`
  ADD PRIMARY KEY (`invite_request_id`);

--
-- Indexes for table `tbl_merchants`
--
ALTER TABLE `tbl_merchants`
  ADD PRIMARY KEY (`merchant_id`);

--
-- Indexes for table `tbl_transactions_receive`
--
ALTER TABLE `tbl_transactions_receive`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_charity`
--
ALTER TABLE `tbl_charity`
  MODIFY `charity_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_gift_invitation`
--
ALTER TABLE `tbl_gift_invitation`
  MODIFY `invite_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_invitation_request`
--
ALTER TABLE `tbl_invitation_request`
  MODIFY `invite_request_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_merchants`
--
ALTER TABLE `tbl_merchants`
  MODIFY `merchant_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_transactions_receive`
--
ALTER TABLE `tbl_transactions_receive`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
