<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include("header.php");
$GiftWalletApi = new Common($dbconn);

// $emptycondn = array();
// // Getting Daily Order count
// $dailyQry = "SELECT count(order_id) as today_order_count FROM `tbl_orders` where date_format(now(),'%Y-%m-%d') between  date_format(start_date,'%Y-%m-%d') and date_format(end_date,'%Y-%m-%d')";
// $getOrderCount = $foodAppApi->funBckendExeSelectQuery($dailyQry,$emptycondn,'fetch');
// $dailycount = getcount($getOrderCount["today_order_count"]);

// // Getting Weekly Order count
// $weekQry = "SELECT count(order_id) as weekly_order_count FROM `tbl_orders` where WEEK(date_format(start_date,'%Y-%m-%d')) = WEEK(date_format(now(),'%Y-%m-%d')) and  WEEK(date_format(end_date,'%Y-%m-%d')) = WEEK(date_format(now(),'%Y-%m-%d'))";
// $getOrderCount = $foodAppApi->funBckendExeSelectQuery($weekQry,$emptycondn,'fetch');
// $weekcount = getcount($getOrderCount["weekly_order_count"]);

// // Getting Monthly Order count
// $monthQry = "SELECT count(order_id) as monthly_order_count from `tbl_orders` where MONTH(date_format(start_date,'%Y-%m-%d')) = MONTH(date_format(now(),'%Y-%m-%d')) and MONTH(date_format(end_date,'%Y-%m-%d')) = MONTH(date_format(now(),'%Y-%m-%d')) and date_format(start_date,'%Y') = date_format(now(),'%Y')";
// $getOrderCount = $foodAppApi->funBckendExeSelectQuery($monthQry,$emptycondn,'fetch');
// $monthcount = getcount($getOrderCount["monthly_order_count"]);

// // Getting Vendor and Customer count
// $vendrcntQry = "SELECT count(vendor_id) as vendor_count,sum(cus_count) as customer_count from (SELECT count(customer_id) as cus_count,vendor_id FROM `tbl_orders` group by vendor_id) as p";
// $getVendorCount = $foodAppApi->funBckendExeSelectQuery($vendrcntQry,$emptycondn,'fetch');
// $vendor_count = getcount($getVendorCount["vendor_count"]);
// $customer_count = getcount($getVendorCount["customer_count"]);

// function getcount($count) {
//     return ($count>9)?$count:'0'.$count;
// }
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->   
    <div class="row widget-row">
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                <h4 class="widget-thumb-heading">Daily</h4>
                <div class="widget-thumb-wrap">
                    <!-- <i class="widget-thumb-icon bg-green icon-bulb"></i> -->
                    <!-- <img src="images/test.png" class="img-responsive"> -->
                    <div class="widget-thumb-body">
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <!-- <span class="widget-thumb-body-stat" data-counter="counterup" data-value="7,644">0</span> -->
                        <table>
                            <tr>
                                <td><img src="../assets/layouts/layout2/img/order.png" class="img-responsive dasboard-order-img" alt="daily-orders"></td>
                                <td>&nbsp;</td>
                                <td><span class="widget-thumb-body-stat" data-counter="counterup" data-value="5">0</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                <h4 class="widget-thumb-heading">Weekly</h4>
                <div class="widget-thumb-wrap">
                    <!-- <i class="widget-thumb-icon bg-red icon-layers"></i> -->
                    <div class="widget-thumb-body">
                        <table>
                            <tr>
                                <td><img src="../assets/layouts/layout2/img/order.png" class="img-responsive dasboard-order-img" alt="weekly-orders"></td>
                                <td>&nbsp;</td>
                                <td><span class="widget-thumb-body-stat" data-counter="counterup" data-value="30">0</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                <h4 class="widget-thumb-heading">Monthly</h4>
                <div class="widget-thumb-wrap">
                    <!-- <i class="widget-thumb-icon bg-purple icon-screen-desktop"></i> -->
                    <div class="widget-thumb-body">
                        <table>
                            <tr>
                                <td><img src="../assets/layouts/layout2/img/order.png" class="img-responsive dasboard-order-img" alt="monthly-orders"></td>
                                <td>&nbsp;</td>
                                <td><span class="widget-thumb-body-stat" data-counter="counterup" data-value="150">0</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                <h4 class="widget-thumb-heading">Vendors/Customers</h4>
                <div class="widget-thumb-wrap">
                    <!-- <i class="widget-thumb-icon bg-blue icon-bar-chart"></i> -->
                    <div class="widget-thumb-body">
                        <table>
                            <tr>
                                <td><img src="../assets/layouts/layout2/img/vendor.png" class="img-responsive dasboard-order-img" alt="vendors-customers"></td>
                                <td>&nbsp;</td>
                                <td><span class="widget-thumb-body-stat" data-counter="counterup" data-value="15/25">0</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>  

    <div class="row food-orders">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
           <!--  <div class="portlet light ">
                <div class="portlet-title" style="margin-bottom: 0px;">

                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Orders</span>
                    </div>
                    <div class="tools"> </div>
                </div>

                <div class="portlet-body" style="padding-top: 0px;">
                    <table class="table table-striped table-bordered table-hover" id="food-orders-list">
                        <thead>
                            <tr>
                                <th>Order no</th>
                                <th>Customer name</th>
                                <th>Order type</th>
                                <th>Order amount</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>-->
                            <?php 
                                // $Qry = "SELECT full_name,order_type,price,ordr.status from tbl_orders as ordr join tbl_users as usr on ordr.customer_id = usr.user_id where lower(usr.user_type) = 'customer'";
                                // $getOrder = $foodAppApi->funBckendExeSelectQuery($Qry);
                                // $ordertbl = ""; $i=1;
                                // if(count($getOrder)>0) {
                                //     foreach($getOrder as $fetchorder) {
                                //         $ordertbl.="<tr>";
                                //         $ordertbl.="<td>".$i++."</td>";
                                //         $ordertbl.= "<td>".$fetchorder['full_name']."</td>";
                                //         $ordertbl.= "<td>".$fetchorder['order_type']."</td>";
                                //         $ordertbl.= "<td>".$fetchorder['price']."</td>";
                                //         $ordertbl.= "<td>".$fetchorder['status']."</td>";
                                //     }
                                // }
                                // if($ordertbl)
                                //     echo $ordertbl;
                                // else 
                                //     echo "<td> No Order(s) found</td>";
                            ?>
                        <!-- </tbody>
                    </table>
                </div>
            </div> -->
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
<?php include_once("footer.php"); ?>