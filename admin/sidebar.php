<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- END SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <?php 
            // echo $navigationURL = end(explode("/",$_SERVER["REQUEST_URI"]));
            if (strpos($_SERVER["REQUEST_URI"], '?') !== false) {
                $url = (explode("?",$_SERVER["REQUEST_URI"]));
                $navigationURL = end(explode("/",$url[0]));
            } else {
                $navigationURL = end(explode("/",$_SERVER["REQUEST_URI"]));
            }
        ?>
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start <?php echo ($navigationURL=='dashboard.php')?'active':''; ?> open side-menu-logo">
                <a href="dashboard.php" class="side-menu nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                    <!-- <span class="arrow open"></span> -->
                </a>
            </li>
            <li class="nav-item start open <?php echo ($navigationURL=='merchant_listing.php')?'active':''; ?> side-menu-logo">
                <a href="merchant_listing.php" class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">Merchants</span>
                    <span class="selected"></span>
                    <!-- <span class="arrow open"></span> -->
                </a>                            
            </li>
            <li class="nav-item start open <?php echo ($navigationURL=='charity_listing.php')?'active':'';?> side-menu-logo">
                <a href="charity_listing.php" class="nav-link nav-toggle">
                    <i class="fa fa-bank"></i>
                    <span class="title">Charities</span>            
                    <span class="selected"></span>
                    <!-- <span class="arrow open"></span> -->
                </a>                            
            </li>
            <li class="nav-item start open <?php echo ($navigationURL=='gift_listing.php')?'active':'';?> side-menu-logo">
                <a href="gift_listing.php" class="nav-link nav-toggle">
                    <i class="fa fa-gift"></i>
                    <span class="title">Gifts</span>            
                    <span class="selected"></span>
                    <!-- <span class="arrow open"></span> -->
                </a>                            
            </li>
            <li class="nav-item start open <?php echo ($navigationURL=='user_listing.php')?'active':'';?> side-menu-logo">
                <a href="user_listing.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Users</span>            
                    <span class="selected"></span>
                    <!-- <span class="arrow open"></span> -->
                </a>                            
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->