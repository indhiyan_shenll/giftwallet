<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
/****Paging ***/
$Page=1;$RecordsPerPage=25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
$TotalPages=0;
/*End of paging*/

$GiftWalletApi = new Common($dbconn);

$qrycondition = "";
$filterArr="";
$from_date = "";
$to_date = "";
$gift_name = "";
$created_by = "";
if (isset($_POST['from_date']) || isset($_POST['to_date']) || isset($_POST['gift_name']) || isset($_POST['created_by'])) {
    $filterArr = array();
    if ($_POST['from_date']!="" || $_POST['to_date']!="" || $_POST['gift_name']!="" || $_POST['created_by']!="") {
        if ((strlen($_POST['from_date'])) > 0 ) {
            $filterArr['from_date'] = date('Y-m-d H:i:s',strtotime($_POST['from_date']));
            $qrycondition .= " AND gift.invite_reg_date>=:from_date";
            $from_date = date('m/d/Y',strtotime($_POST['from_date']));
        }
        if ((strlen($_POST['to_date'])) > 0 ) {
            $filterArr['to_date'] = date('Y-m-d H:i:s',strtotime($_POST['to_date']));
            $qrycondition .= " AND gift.invite_reg_date<=:to_date";
            $to_date = date('m/d/Y',strtotime($_POST['to_date']));
        }
        if ((strlen($_POST['gift_name'])) > 0 ) {
            $qrycondition .= " AND gift.invite_name like '%".trim($_POST['gift_name'])."%'";
             $gift_name = $_POST['gift_name'];
        }
        if ((strlen($_POST['created_by'])) > 0 ) {
            $filterArr['created_by'] = $_POST['created_by'];
            $qrycondition .= " AND gift.admin_id = :created_by ";
            $created_by = $_POST['created_by'];
        }
    } else {
        $qrycondition = "";
        $filterArr="";
        $from_date = "";
        $to_date = "";
        $gift_name = "";
        $created_by = "";
    }
} else {
    $qrycondition = "";
    $filterArr="";
    $from_date = "";
    $to_date = "";
    $gift_name = "";
    $created_by = "";
}

include("header.php");
?>
<form name="giftlist_form" id="giftlist_form" method="post" action="">
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<!-- overwrtite the css for this particular page -->
<style type="text/css">
    .add_Gift{min-height: 34px !important;}
    .status{font-weight: bolder;}
    .reportcustomersearch,.table-scrollable{margin-top: 20px !important;}
    .table.dataTable thead:first-child .sorting_asc{background: none !important;}
    table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting{padding: 8px 10px !important;}
    .remove-padding{
        padding-left: 0px;
        padding-right: 0px;
    }
    .search-orderlist-btns{
        margin-top: 22px;
        padding: 0px;
    }
    .custombtn {
        width: 95px;
        border-radius: 6px !important;
        box-shadow: 0px 3px 1px #888888 !important;
    }
    #tbl_gift_list thead tr th:nth-child(1), th:nth-child(7){
        text-align: center;
    }
    #tbl_gift_list tbody tr td:nth-child(1), td:nth-child(7){
        text-align: center;
    }
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- <?php if ($msg!="") { ?>
        <div class="custom-alerts alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><?php echo $msg_display; ?></div>
    <?php } ?> -->
	<div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light">
                <div class="portlet-title" style="margin-bottom: 0px;">

                    <div class="caption font-dark">
                        <i class="fa fa-gift font-dark"></i>
                        <span class="caption-subject bold uppercase">Gifts</span>
                    </div>
                    <div class="tools">
                        <!-- <a href="manage_gifts.php" class="btn btn-circle btn-outline red dropdown-toggle add_Gift">
                            Add Gift&nbsp;
                        </a> -->
                    </div>
                </div>

                <div class="portlet-body flip-scroll">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch">
                            <div class="col-md-9 col-sm-9 col-xs-12 remove-padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <label>Gift Name:</label>
                                    <input type="text" name="gift_name" id="gift_name" class="form-control form-control-inline" placeholder="Gift Name" value="<?php echo $gift_name; ?>">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <label>From Date:</label>
                                    <input type="text" name="from_date" id="from_date" class="form-control form-control-inline  date-picker" placeholder="From Date" value="<?php echo $from_date; ?>">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <label>To Date:</label>
                                    <input type="text" name="to_date" id="to_date" class="form-control form-control-inline date-picker" placeholder="To Date" value="<?php echo $to_date; ?>">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <label>Created By:</label>
                                    <select name="created_by" id="created_by" class="form-control">
                                        <option value="">Select</option>
                                        <?php
                                            $createdByQry="SELECT user_id,first_name,last_name from tbl_users where user_id in (SELECT DISTINCT(admin_id) FROM tbl_gift_invitation as gift left join tbl_users as user on gift.admin_id=user.user_id) order by user_id desc";
                                            $createdByqryParams[":status"]="1";
                                            $getCustomers = $GiftWalletApi->funBckendExeSelectQuery($createdByQry,$createdByqryParams);
                                            if (count($getCustomers,COUNT_RECURSIVE)>1) {
                                                foreach ($getCustomers as $getCustomerData) {
                                                    if ($created_by==$getCustomerData["user_id"])
                                                        $selected = 'selected';
                                                    else
                                                        $selected = '';
                                                    echo "<option value=".$getCustomerData["user_id"]." ".$selected.">".$getCustomerData["first_name"]." ".$getCustomerData["last_name"]."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 remove-padding">
                                <div class="col-md-12 col-sm-12 col-xs-12 search-orderlist-btns">
                                    <button type="button" class="btn yellow custombtn" id="search"><i class="fa fa-search" aria-hidden="true"></i> Search</button>  
                                    <a type="button" href="gift_listing.php" class="btn red custombtn"><i class="fa fa-times-circle" aria-hidden="true"></i> Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped table-condensed flip-content" id="tbl_gift_list">
                        <thead class="flip-content">
                            <tr>
                            	<th width="5%">S.No</th>
                                <th nowrap>Gift Name</th>
                                <th nowrap>Created By</th>
                                <th>Created Date</th>
                                <th>Code</th>
                                <th>Message</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
	                            $Qry="SELECT * FROM tbl_gift_invitation as gift left join tbl_users as user on gift.admin_id=user.user_id where invite_id <>'' $qrycondition order by invite_id desc";
                                if ($filterArr!="") {
                                    $qryParams = $filterArr;
                                    $getResCnt = $GiftWalletApi->funBckendExeSelectQuery($Qry,$qryParams);
                                }
                                else
                                    $getResCnt = $GiftWalletApi->funBckendExeSelectQuery($Qry);
                                
                                // echo $Qry;
                                if (count($getResCnt,COUNT_RECURSIVE)>1) {
                                    $TotalPages=ceil(count($getResCnt)/$RecordsPerPage);
                                    $Start=($Page-1)*$RecordsPerPage;
                                    $sno=$Start+1;
                                    $Qry.=" limit $Start,$RecordsPerPage";
                                    if ($filterArr!="") {
                                        $qryParams = $filterArr;
                                        $getGift = $GiftWalletApi->funBckendExeSelectQuery($Qry,$qryParams);
                                    }
                                    else
                               		   $getGift = $GiftWalletApi->funBckendExeSelectQuery($Qry);

							   	    if (count($getGift)>0) {
							   		    foreach ($getGift as $GiftListData) {
							?>
						       <tr>
						       		<td><?php echo $sno;?></td>
						       		<td><?php echo $GiftListData["invite_name"];?></td>
                                    <td><?php echo $GiftListData["first_name"]." ".$GiftListData["last_name"];?></td>
						       		<td><?php echo date('m-d-Y',strtotime($GiftListData["invite_reg_date"]));?></td>
						       		<td><?php echo $GiftListData["code"];?></td>
						       		<td><?php echo $GiftListData["message"];?></td>
						       		<td>
						       			<div class="actions">
										    <div class="btn-group">
										        <a class="btn btn-xs default" href="gift_details.php?id=<?php echo base64_encode($GiftListData["invite_id"]); ?>" > View
										            <i class="fa fa-angle-right"></i>
										        </a>
										    </div>
										</div>
									</td>
						       </tr>
                            <?php   
							    $sno++;
						            }
								} else {
                                    echo "<tr><td colspan='8' style='text-align:center;'>No record(s) found </td></tr>";
								}
                            } else {
                                echo "<tr><td colspan='8' style='text-align:center;'>No record(s) found </td></tr>";
                            }

                            ?>
                            
                        </tbody>
                    </table>
                </div>
                <div>
                    <?php
                        if($TotalPages > 1){

                            echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
                            $FormName = "giftlist_form";
                            require_once ("paging.php");
                            echo "</td></tr>";
                        }
                    ?>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
</div>
</form>
<?php include_once("footer.php"); ?>
<script>
$('#tbl_gift_list').DataTable( {
    "bPaginate": false,
    "bFilter": false,
    "bInfo": false,
    "iDisplayLength": false ,
   'aoColumnDefs': [
        {'aTargets': [9],'bSortable': false,
        'aTargets': [0, 6],'bSortable': false,},
    ],
     "order": [[ 0, "asc" ]]  
} ); 

$("#search").click(function() {
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var gift_name = $('#gift_name').val();
    // $('select[id=created_by]').val()
    var created_by = $('select[id=created_by]').val();
    var filterArr = [];
    filterArr['from_date']=from_date;
    filterArr['to_date']=to_date;
    filterArr['gift_name']=gift_name;
    filterArr['created_by']=created_by;
    console.log(filterArr);
    if (filterArr['from_date']=="" && filterArr['to_date']=="" && filterArr['gift_name']=="" && filterArr['created_by']=="") {
        // $('#giftlist_form').submit();
    } else {
        $('#giftlist_form').submit();
    }
});
</script>