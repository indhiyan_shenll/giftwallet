<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
/****Paging ***/
$Page=1;$RecordsPerPage=3;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
$TotalPages=0;
/*End of paging*/

$GiftWalletApi = new Common($dbconn);

$DeleteId="";$msg="";
if(isset($_GET['id']) && !empty($_GET['id'])){
    $Id=base64_decode($_GET['id']);

    $Qry="SELECT * FROM tbl_users where user_id=:user_id";
    $qryParams['user_id']=$Id;
    $getResCnt = $GiftWalletApi->funBckendExeSelectQuery($Qry,$qryParams);
    if (count($getResCnt)>0) {
        foreach ($getResCnt as $UserListData) {
            $user_id = $UserListData['user_id'];
            $username = $UserListData['first_name']." ".$UserListData['last_name'];
            $address = $UserListData['address'];
            $email = $UserListData['email'];
            $mobile_number = $UserListData['mobile_number'];
            $country = $UserListData['country'];
            $statusColor=($UserListData["status"]=="1")?"green":"red";
            $status=($UserListData["status"]=="1")?"Active":"In-Active";
        }
    }
}

include("header.php");
?>
<form name="merchantlist_form" id="merchantlist_form" method="post" action="">
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<!-- overwrtite the css for this particular page -->
<style type="text/css">
    .add_merchant{min-height: 34px !important;}
    .status{font-weight: bolder;}
    .table-scrollable{margin-top: 20px !important;}
    .gift_name{margin-top: 0px !important;margin-bottom: 20px !important;font-weight: bolder;}
    .head {font-weight: bolder;text-align: right !important;}
    .user-name{text-align: center;font-weight: bolder;}
    .status{font-weight: bolder;}
    #tbl_merchant_list thead tr th:nth-child(1){
        text-align: center;
    }
    #tbl_merchant_list thead tr th:nth-child(5){
        text-align: right;
    }
    #tbl_merchant_list tbody tr td:nth-child(1){
        text-align: center;
    }
    #tbl_merchant_list tbody tr td:nth-child(5){
        text-align: right;
    }
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!--  <?php if ($msg!="") { ?>
        <div class="custom-alerts alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><?php echo $msg_display; ?></div>
    <?php } ?> -->
	<div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light">
                <div class="portlet-title" style="margin-bottom: 0px;">

                    <div class="caption font-dark">
                        <i class="icon-users font-dark"></i>
                        <span class="caption-subject bold uppercase">User Details</span>
                    </div>
                    <div class="tools">
                        <!-- <a href="manage_merchants.php" class="btn btn-circle btn-outline red dropdown-toggle add_merchant">
                            Add Merchant&nbsp;
                        </a> -->
                    </div>
                </div>

                <div class="portlet-body flip-scroll">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch">
                            <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8 details">
                                <div class="col-md-12">                                    
                                    <h3 class="control-label user-name"><?php echo $username; ?></h3>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="control-label head">Address :</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 bottom_filters">
                                        <p class="control-label"><?php echo $address; ?></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="control-label head">Email :</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 bottom_filters">
                                        <p class="control-label"><?php echo $email; ?></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="control-label head">Phone :</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 bottom_filters">
                                        <p class="control-label"><?php echo $mobile_number; ?></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="control-label head">Country :</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 bottom_filters">
                                        <p class="control-label"><?php echo $country; ?></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="control-label head">Status :</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 bottom_filters">
                                        <p class="control-label status" style="color:<?php echo $statusColor?>"><?php echo $status; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped table-condensed flip-content" id="tbl_merchant_list">
                        <thead class="flip-content">
                            <tr>
                            	<th width="5%">S.No</th>
                                <th nowrap>Invite Name</th>
                                <th>Request Date</th>
                                <th>Receive Date</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
	                            $ReqResQry="SELECT * FROM tbl_invitation_request AS req LEFT JOIN tbl_transactions_receive AS res ON req.invite_request_id=res.invite_request_id LEFT JOIN tbl_gift_invitation AS gift ON req.invite_id=gift.invite_id LEFT JOIN tbl_users AS user ON req.requested_user_id=user.user_id LEFT JOIN tbl_users as users on users.user_id=req.requested_user_id WHERE req.invite_id=:invite_id";
                                $ReqResqryParams['invite_id']=$Id;
	                            $getReqResCnt = $GiftWalletApi->funBckendExeSelectQuery($ReqResQry,$ReqResqryParams);
                                // print_r($Id);
                                if (count($getReqResCnt,COUNT_RECURSIVE)>1) {
                                    $TotalPages=ceil(count($getReqResCnt)/$RecordsPerPage);
                                    $Start=($Page-1)*$RecordsPerPage;
                                    $sno=$Start+1;
                                    $Qry.=" limit $Start,$RecordsPerPage";
                               		$getReqRes = $GiftWalletApi->funBckendExeSelectQuery($ReqResQry,$ReqResqryParams);

                                    // print_r($getReqRes);
							   	    if (count($getReqRes)>0) {
							   		    foreach ($getReqRes as $UserListData) {
                                            // print_r($ReqResListData);
							   			   $payment_statusColor=($UserListData["payment_status"]=="Completed")?"green":"red";
                                           $payment_status=($UserListData["payment_status"]=="Completed")?"Completed":"-";
							?>
						       <tr>
						       		<td><?php echo $sno;?></td>
						       		<td><?php echo $UserListData["first_name"]." ".$UserListData["last_name"];?></td>
                                    <?php $request_date = $UserListData["request_date"];?>
                                    <td><?php echo date('m/d/Y' , strtotime($request_date));?></td>
                                    <?php $payment_date = $UserListData["payment_date"];?>
						       		<td><?php echo date('m/d/Y' , strtotime($payment_date));?></td>
						       		<td><?php echo $UserListData["amount"];?></td>
						       </tr>
                            <?php   
							    $sno++;
						            }
								} else {
                                    echo "<tr><td colspan='10' style='text-align:center;'>No record(s) found </td></tr>";
								}
                            } else {
                                echo "<tr><td colspan='10' style='text-align:center;'>No record(s) found </td></tr>";
                            }
                            ?>
                            
                        </tbody>
                    </table>
                </div>
                <div>
                    <?php
                        if($TotalPages > 1){

                            echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
                            $FormName = "merchantlist_form";
                            require_once ("paging.php");
                            echo "</td></tr>";
                        }
                    ?>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
</div>
</form>
<?php include_once("footer.php"); ?>
<script>
// $('#tbl_merchant_list').DataTable( {
//         "bPaginate": false,
//         "bFilter": false,
//         "bInfo": false,
//         "iDisplayLength": false ,
//        'aoColumnDefs': [
//             {'aTargets': [9],'bSortable': false,
//             'aTargets': [0],'bSortable': false,},
//         ],
//          "order": [[ 0, "asc" ]]  
//     } );    
</script>