<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
/****Paging ***/
$Page=1;$RecordsPerPage=3;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
$TotalPages=0;
/*End of paging*/

$GiftWalletApi = new Common($dbconn);

$DeleteId="";$msg="";
if(isset($_GET['id']) && !empty($_GET['id'])){
    $TblName = "tbl_users";
    $qryParams['user_id']=base64_decode($_GET['id']);
    $whereCndn = " WHERE user_id=:user_id";

    $getDeleteRes = $GiftWalletApi->funExeDeleteRecord($TblName,$qryParams,$whereCndn);

    if ($getDeleteRes==1) {
        header("Location: user_listing.php?msg=delete");
        exit;
    }
    $msg = "deleted";
}

if(isset($_GET['msg']) && !empty($_GET['msg'])){
    $msg=$_GET['msg'];
    if ($msg=='delete') {
        $msg_display = 'Users details deleted successfully';
    } else if ($msg=='update') {
        $msg_display = 'Users details updated successfully';
    }  else if ($msg=='insert') {
        $msg_display = 'Users details added successfully';
    }
}


$qrycondition = "";
$filterArr="";
$user_name = "";
$user_email = "";
$user_status = "";
if (isset($_POST['user_name']) || isset($_POST['user_email']) || isset($_POST['user_status'])) {
    $filterArr = array();
    if ($_POST['user_name']!="" || $_POST['user_email']!="" || $_POST['user_status']!="") {        
        
        if ((strlen($_POST['user_name'])) > 0 ) {
            $qrycondition .= " AND first_name like '%".trim($_POST['user_name'])."%'";
            $qrycondition .= " OR last_name like '%".trim($_POST['user_name'])."%'";
            $user_name = $_POST['user_name'];
        }
        if ((strlen($_POST['user_email'])) > 0 ) {
            $qrycondition .= " AND email like '%".trim($_POST['user_email'])."%'";
            $user_email = $_POST['user_email'];
        }
        if ((strlen($_POST['user_status'])) > 0 ) {
            $qrycondition .= " AND status like '%".trim($_POST['user_status'])."%'";
            $user_status = $_POST['user_status'];
        }        
    } else {
        $qrycondition = "";
        $filterArr="";
        $user_name = "";
        $user_email = "";
        $user_status = "";
    }
} else {
    $qrycondition = "";
    $filterArr="";
    $user_name = "";
    $user_email = "";
    $user_status = "";
}

include("header.php");
?>
<form name="userlist_form" id="userlist_form" method="post" action="">
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<!-- overwrtite the css for this particular page -->
<style type="text/css">
    .add_merchant{min-height: 34px !important;}
    .status{font-weight: bolder;}
    .search-orderlist-btns{
        margin-top: 22px;
        padding: 0px;
    }
    .remove-padding{
        padding-left: 0px;
        padding-right: 0px;
    }
    .custombtn {
        width: 95px;
        border-radius: 6px !important;
        box-shadow: 0px 3px 1px #888888 !important;
    }
    .table-scrollable {
        margin-top: 20px !important;
    }    
    #tbl_merchant_list tbody tr td{
        vertical-align: middle;
    }
    #tbl_merchant_list tbody tr td:nth-child(4){
        white-space: nowrap;
    }
    #tbl_merchant_list tbody tr td:nth-child(1), td:nth-child(6), td:nth-child(7) {
        text-align: center;
        vertical-align: middle;
    }    
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <?php if ($msg!="") { ?>
        <div class="custom-alerts alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><?php echo $msg_display; ?></div>
    <?php } ?>
	<div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light">
                <div class="portlet-title" style="margin-bottom: 0px;">

                    <div class="caption font-dark">
                        <i class="fa fa-users font-dark"></i>
                        <span class="caption-subject bold uppercase">Users</span>
                    </div>
                    <div class="tools">
                        <!-- <a href="manage_merchants.php" class="btn btn-circle btn-outline red dropdown-toggle add_merchant">
                            Add User&nbsp;
                        </a> -->
                    </div>
                </div>

                <div class="portlet-body flip-scroll">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch">
                            <div class="col-md-8 col-sm-8 col-xs-12 remove-padding">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>User Name:</label>
                                    <input type="text" name="user_name" id="user_name" class="form-control form-control-inline" placeholder="User Name" value="<?php echo $user_name; ?>">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>Email:</label>
                                    <input type="text" name="user_email" id="user_email" class="form-control form-control-inline" placeholder="Email" value="<?php echo $user_email; ?>">
                                </div>
                                 <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>Status:</label>
                                    <select name="user_status" id="user_status" class="form-control form-control-inline">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo (isset($user_status) && $user_status == '1') ? 'selected' : ''; ?>>Active</option>
                                        <option value="0" <?php echo (isset($user_status) && $user_status == '0') ? 'selected' : ''; ?>>In-Active</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 remove-padding">
                                <div class="col-md-12 col-sm-12 col-xs-12 search-orderlist-btns">
                                    <button type="button" class="btn yellow custombtn" id="search"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
                                    <a type="button"  href="user_listing.php" class="btn red custombtn"><i class="fa fa-times-circle" aria-hidden="true"></i> Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped table-condensed flip-content" id="tbl_merchant_list">
                        <thead class="flip-content">
                            <tr>
                            	<th width="5%">S.No</th>
                                <th nowrap>User</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                            $Qry="SELECT * FROM tbl_users where user_id <>'' $qrycondition order by user_id desc";
                                if ($filterArr!="") {
                                    $qryParams = $filterArr;
                                    $getResCnt = $GiftWalletApi->funBckendExeSelectQuery($Qry,$qryParams);
                                }
                                else
                                    $getResCnt = $GiftWalletApi->funBckendExeSelectQuery($Qry);

	                            $getResCnt = $GiftWalletApi->funBckendExeSelectQuery($Qry);
                                // print_r($getResCnt);
                                if (count($getResCnt,COUNT_RECURSIVE)>1) {
                                    $TotalPages=ceil(count($getResCnt)/$RecordsPerPage);
                                    $Start=($Page-1)*$RecordsPerPage;
                                    $sno=$Start+1;
                                    $Qry.=" limit $Start,$RecordsPerPage";
                               		$getUser = $GiftWalletApi->funBckendExeSelectQuery($Qry);

							   	    if (count($getUser)>0) {
							   		    foreach ($getUser as $UserListData) {
							   			   $statusColor=($UserListData["status"]=="1")?"#659be0":"#9c3b3b";
                                           $status=($UserListData["status"]=="1")?"Active":"In-Active";
							?>
						       <tr>
						       		<td><?php echo $sno;?></td>
						       		<td><?php echo $UserListData["first_name"]." ".$UserListData["last_name"];?></td>
                                    <td><?php echo $UserListData["email"];?></td>
						       		<td><?php echo $UserListData["mobile_number"];?></td>
                                    <td><?php echo $UserListData["address"];?></td>
						       		<td class="status"><span class="label label-info" style="background-color:<?php echo $statusColor?>"><?php echo $status;?></span></td>
						       		<td>
                                        <div class="actions">
                                            <div class="btn-group">
                                                <a class="btn btn-xs default" href="user_details.php?id=<?php echo base64_encode($UserListData["user_id"]); ?>" > View
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
						       </tr>
                            <?php   
							    $sno++;
						            }
								} else {
                                    echo "<tr><td colspan='10' style='text-align:center;'>No record(s) found </td></tr>";
								}
                            } else {
                                echo "<tr><td colspan='10' style='text-align:center;'>No record(s) found </td></tr>";
                            }

                            ?>
                            
                        </tbody>
                    </table>
                </div>
                <div>
                    <?php
                        if($TotalPages > 1){

                            echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
                            $FormName = "userlist_form";
                            require_once ("paging.php");
                            echo "</td></tr>";
                        }
                    ?>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
</div>
</form>
<?php include_once("footer.php"); ?>
<script>
$('#tbl_merchant_list').DataTable( {
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength": false ,
       'aoColumnDefs': [
            {'aTargets': [9],'bSortable': false,
            'aTargets': [0, 5, 6],'bSortable': false,},
        ],
         "order": [[ 0, "asc" ]]  
    } );
    $("#search").click(function() {
        var user_name = $('#user_name').val();
        var user_email = $('#user_email').val();
        var user_status = $('#user_status').val();
        var filterArr = [];
        filterArr['user_name']=user_name;
        filterArr['user_email']=user_email;
        filterArr['user_status']=user_status;
       
        if (filterArr['user_name']=="" && filterArr['user_email']=="" && filterArr['user_status']=="") {
            // $('#giftlist_form').submit();
        } else {
            $('#userlist_form').submit();
        }
    });
</script>