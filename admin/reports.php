<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
//include("header.php");
$foodAppApi = new Common($dbconn);

if(isset($_GET["cusid"])){
	$customer_id=base64_decode($_GET["cusid"]);
	$Qry="SELECT users.full_name,orders.order_id,orders.vendor_id,orders.start_date,orders.end_date,orders.price,orders.ratings,orders.order_type,orders.status FROM tbl_orders as orders INNER JOIN tbl_users as users ON users.user_id=orders.vendor_id where orders.customer_id=:customerid  order by orders.order_id desc";
    $qryParams[":customerid"]=$customer_id;

} else {
    $Qry="SELECT users.full_name,orders.order_id,orders.vendor_id,orders.start_date,orders.end_date,orders.price,orders.ratings,orders.order_type,orders.status FROM tbl_orders as orders INNER JOIN tbl_users as users ON users.user_id=orders.vendor_id where orders.customer_id !=''  order by orders.order_id desc";
    $qryParams[":empty"]="";
}
/****Paging ***/
$Page=1;$RecordsPerPage=3;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
/*End of paging*/
include("header.php");
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title" style="margin-bottom: 0px;">

                    <div class="caption font-dark">
                        <i class="icon-layers font-dark"></i>
                        <span class="caption-subject bold uppercase">Reports</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                	<div class="row">
	                	
	                    <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch">
		                	<div class="col-md-3 col-sm-3 col-xs-12">
		                		<label>From Date:</label>
		                		<input type="text" name="form_date" id="form_date" class="form-control form-control-inline  date-picker" placeholder="From Date">
							</div>
							<div class="col-md-3 col-sm-3 col-xs-12">
		                		<label>To Date:</label>
		                		<input type="text" name="to_date" id="to_date" class="form-control form-control-inline  date-picker" placeholder="To Date">
							</div>
							<div class="col-md-3 col-sm-3 col-xs-12">
		                		<label>Vendors Name:</label>
		                		<select name="vendors" id="vendors" class="form-control">
		                			<option value="">---select---</option>
		                			<?php
			                			$Qry1="SELECT * FROM tbl_users where user_type=:user_type and status=:status order by user_id desc";
			                            $qryParams1[":user_type"]="vendor";
			                            $qryParams1[":status"]="Active";
			                            $getvendors = $foodAppApi->funBckendExeSelectQuery($Qry1,$qryParams1);
			                            if (count($getvendors,COUNT_RECURSIVE)>1) {
				                            foreach ($getvendors as $getVendorsData) {
				                            	echo "<option value=".$getVendorsData["user_id"].">".$getVendorsData["full_name"]."</option>";
				                            }
				                        }
		                			?>
		                		</select>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-12">
		                		<label>Customer Name:</label>
		                		<select name="customer" id="customer" class="form-control">
		                			<option value="">---select---</option>
		                			<?php
			                			$Qry2="SELECT * FROM tbl_users where user_type=:user_type and status=:status order by user_id desc";
			                            $qryParams2[":user_type"]="customer";
			                            $qryParams2[":status"]="Active";
			                            $getCustomers = $foodAppApi->funBckendExeSelectQuery($Qry2,$qryParams2);
			                            if (count($getCustomers,COUNT_RECURSIVE)>1) {
				                            foreach ($getCustomers as $getCustomerData) {
				                            	if($getCustomerData["user_id"]==$customer_id)
				                            		$selected="selected";
				                            	else
				                            		$selected="";
				                            	echo "<option value=".$getCustomerData["user_id"]." ".$selected.">".$getCustomerData["full_name"]."</option>";
				                            }
				                        }
		                			?>
		                		</select>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch text-center">
	                		<button type="button" class="btn green btn-md" id="Search">Search</button>  
	                		 
	                		<?php
	                		if (!empty($customer_id)) {
	                		?>
	                		<a type="button"  href="reports.php?cusid=<?php echo base64_encode($customer_id);?>" class="btn red btn-md">Reset</a>
	                	    <a class="btn yellow btn-md" href="customer_listing.php">Back</a>
	                		<?php
	                		} else{
	                		?>
	                		<a type="button"  href="reports.php" class="btn red btn-md">Reset</a>
	                		<?php
							}
	                		?>
	                		
	                	</div>
					</div>
                </div>
                <form name="reportlist_form" id="reportlist_form" method="post" action="" >
				<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
				<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
				<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
	                <div class="portlet-body flip-scroll" id="sample">
	                    <table class="table table-bordered table-striped table-condensed flip-content" id="tbl_reports_list">
	                        <thead class="flip-content">
	                            <tr>
	                            	<th width="5%">Id</th>
	                                <th nowrap>Vendor Name</th>
	                                <th>Order Id</th>
	                                <th>Start Date</th>
	                                <th>End Date</th>
	                                <th>Rating</th>
	                                <th>Price</th>
	                                <th>Status</th>
	                               <!--  <th>Action</th> -->
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	<?php
		                            $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
		                            if (count($getResCnt,COUNT_RECURSIVE)>1) {
	                                    $TotalPages=ceil(count($getResCnt)/$RecordsPerPage);
	                                    $Start=($Page-1)*$RecordsPerPage;
	                                    $sno=$Start+1;
	                                    $Qry.=" limit $Start,$RecordsPerPage";
	                               		$getReports = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
	                           		    //$sno=1;
								   	    if (count($getReports)>0) {
								   		    foreach ($getReports as $reportsListData) {
								   			   $statusColor=(strtolower($reportsListData["status"])=="pending")?"red":"green";

								?>
								       <tr>
								       		<td><?php echo $sno;?></td>
								       		<td><?php echo $reportsListData["full_name"];?></td>
								       		<td><?php echo $reportsListData["order_id"];?></td>
								       		<td><?php echo date("d/m/Y",strtotime($reportsListData["start_date"]));?></td>
								       		<td><?php echo date("d/m/Y",strtotime($reportsListData["end_date"]));?></td>
								       		<td><?php echo $reportsListData["price"];?></td>
								       		<td><?php echo $reportsListData["ratings"];?></td>
								       		<td style="color:<?php echo $statusColor?>"><?php echo $reportsListData["status"];?></td>
								       		<!-- <td>
								       			<div class="actions">
												    <div class="btn-group">
												        <a class="btn dark btn-sm" href="order_item.php?ordid=<?php echo base64_encode($reportsListData["order_id"]);?>">View Order
												          
												        </a>
												    </div>
												</div>
											</td>--> 
								       </tr>
	                            <?php   
								    $sno++;		
							            }
									} else {
	                                    echo "<tr><td colspan='9' style='text-align:center;'>No report(s) found </td></tr>";
									}
	                            } else {
	                                echo "<tr><td colspan='9' style='text-align:center;'>No report(s) found </td></tr>";
	                            }

	                            ?>
	                            
	                        </tbody>
	                    </table>
	                
	                </div>
	                <div>
	                    <?php
	                        if($TotalPages > 1){

	                            echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
	                            $FormName = "reportlist_form";
	                            require_once ("paging.php");
	                            echo "</td></tr>";
	                        }
	                    ?>
	                </div>
            	</form>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
</div>
<?php include("footer.php");?>
<style>
.dataTables_extended_wrapper .table.dataTable{
	margin:0px 0!important;;
}
	
</style>
<script>
$(document).ready(function() {
  loading_sorting();
});
function loading_sorting(){
	$('#tbl_reports_list').DataTable( {
	        "bPaginate": false,
	        "bFilter": false,
	        "bInfo": false,
	        "iDisplayLength": false ,
	       'aoColumnDefs': [
	            {'aTargets': [7],'bSortable': false,}
	        ],
	         "order": [[ 0, "asc" ]]  
	} );    
}
</script>

<!--****ajax search*******-->
<script>
	$(".dataTables_extended_wrapper .table.dataTable").css("margin","0px !important;");
	$(document).on("click","#Search",function() {
		var formdate   = $("#form_date").val();
		var todate     = $("#to_date").val();
		var vendorsid  = $("#vendors").val();
		var customerid = $("#customer").val();
		var HdnPage        = $("#HdnPage").val();
        var HdnMode        = $("#HdnMode").val();
        var RecordsPerPage = $("#RecordsPerPage").val();
		console.log(formdate);
		console.log(todate);
		console.log(vendorsid);
		console.log(customerid);

		filter_by_customer_reports(formdate,todate,vendorsid,customerid,HdnPage,HdnMode,RecordsPerPage);

	});

	function filter_by_customer_reports(formdate,todate,vendorsid,customerid,HdnPage,HdnMode,RecordsPerPage) {
    	//$(".loadingplayersection").show();
        $("#sample").hide();
        $.ajax({
            url:"ajax_customer_order_report.php",
            method:'GET',
            data:"fromdate="+formdate+"&todate="+todate+"&vendorsid="+vendorsid+"&customerid="+customerid+"&HdnPage="+HdnPage+"&HdnMode="+HdnMode+"&PerPage="+RecordsPerPage,
            success:function(data) { 
                $(".loadingplayersection").hide();
                $("#sample").show();                           
                document.getElementById('sample').innerHTML = data; 
                $('.table-header').remove();
                loading_sorting();
            }
        });                
    }
</script>

