<?php 
include_once('../includes/configure.php');
include_once('../api/Common.php');

$GiftWalletApi = new Common($dbconn);

$alert_message = '';
$alert_class = '';
if (isset($_GET['msg'])) { 
    if ($_GET['msg'] == 2) {
        $alert_message = "Duplicate game!!";
        $alert_class = "alert-danger";
    }
}

$mode="Add";
$charity_id = "";
$charity_name = "";
$owner = "";
$description = "";
$email = "";
$mobile_number = "";
$address = "";
$city = "";
$country = "";
$zipcode = "";
$status = "";

$Id = '';
if (isset($_GET['id'])) {
    $Id = base64_decode($_GET['id']);

    $mode="";$getResCnt="";
    $Qry="SELECT * FROM tbl_charity where charity_id=:charity_id";
    $qryParams['charity_id']=$Id;
    $getResCnt = $GiftWalletApi->funBckendExeSelectQuery($Qry,$qryParams);
    if (count($getResCnt)>0) {
        // print_r($getResCnt);
        foreach ($getResCnt as $MerchantListData) {
            $charity_id = $MerchantListData['charity_id'];
            $charity_name = $MerchantListData['charity_name'];
            $owner = $MerchantListData['owner'];
            $description = $MerchantListData['description'];
            $email = $MerchantListData['email'];
            $mobile_number = $MerchantListData['mobile_number'];
            $address = $MerchantListData['address'];
            $city = $MerchantListData['city'];
            $country = $MerchantListData['country'];
            $zipcode = $MerchantListData['zipcode'];
            $status = $MerchantListData['status'];
        }
        $mode="Edit";
    } else {
        $mode="Add";
        $charity_id = "";
        $charity_name = "";
        $owner = "";
        $description = "";
        $email = "";
        $mobile_number = "";
        $address = "";
        $city = "";
        $country = "";
        $zipcode = "";
        $status = "";
    }
}


if (isset($_POST['submit'])) {
    // print_r($_POST);exit;
    $mode = $_POST['mode'];
    $charity_id = $_POST['charity_id'];
    $charity_name = $_POST['charity_name'];
    $owner = $_POST['owner'];
    $description = $_POST['description'];
    $email = $_POST['email'];
    $mobile_number = $_POST['mobile_number'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $country = $_POST['country'];
    $zipcode = $_POST['zipcode'];
    $status = $_POST['status'];
    $created_date = date("Y-m-d H:i:s");

    if ($mode=='edit') {
        $UpdateQry="UPDATE tbl_charity SET charity_name = :charity_name, owner = :owner, description = :description, email = :email, mobile_number = :mobile_number, address = :address, city = :city, country = :country, zipcode = :zipcode, status = :status WHERE charity_id = :charity_id";
        $UpdateqryParams['charity_name']=$charity_name;
        $UpdateqryParams['owner']=$owner;
        $UpdateqryParams['description']=$description;
        $UpdateqryParams['email']=$email;
        $UpdateqryParams['mobile_number']=$mobile_number;
        $UpdateqryParams['address']=$address;
        $UpdateqryParams['city']=$city;
        $UpdateqryParams['country']=$country;
        $UpdateqryParams['zipcode']=$zipcode;
        $UpdateqryParams['status']=$status;
        $UpdateqryParams['charity_id']=$charity_id;

        $getUpdateqryRes = $GiftWalletApi->funBckendExeUpdateRecord($UpdateQry,$UpdateqryParams);
        // print_r($getUpdateqryRes);exit;
        if ($getUpdateqryRes==1) {
            header("Location: charity_listing.php?msg=update");
            exit;
        }
    } else {
        $InsertQry="INSERT INTO tbl_charity (charity_name, owner, description, email, mobile_number, address, city, country, zipcode, status, created_date, modified_date) VALUES (:charity_name, :owner, :description, :email, :mobile_number, :address, :city, :country, :zipcode, :status, :created_date, :created_date)";
        $InsertqryParams['charity_name']=$charity_name;
        $InsertqryParams['owner']=$owner;
        $InsertqryParams['description']=$description;
        $InsertqryParams['email']=$email;
        $InsertqryParams['mobile_number']=$mobile_number;
        $InsertqryParams['address']=$address;
        $InsertqryParams['city']=$city;
        $InsertqryParams['country']=$country;
        $InsertqryParams['zipcode']=$zipcode;
        $InsertqryParams['status']=$status;
        $InsertqryParams['created_date']=$created_date;
        $InsertqryParams['modified_date']=$created_date;

        $getInsertqryRes = $GiftWalletApi->funBckendExeInsertRecord($InsertQry,$InsertqryParams);
        // print_r($getInsertqryRes);exit;
        if ($getInsertqryRes!="") {
            header("Location: charity_listing.php?msg=insert");
            exit;
        }
    }
}

include_once('header.php'); ?>

<style>
.game-info-caption{padding: 3px 20px 3px !important;}
.error{color: #e73d4a !important;}
.row{padding: 6px 0 !important;}
.top-padding{padding: 0px !important;}
.portlet{margin-bottom: 0px;}
.input-sm{font-size: 14px;}
</style>

    <div class="page-content-wrapper">
        <div class="page-content">
            <?php if (isset($_GET['msg'])) { ?>
            <div class="alert alert-block fade in <?php echo $alert_class; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $alert_message; ?> </p>
            </div>
            <?php } ?>
            <div class="row">
                <form id="charity_form" method="POST">
                    <input type="hidden" id="charity_id" value="<?php echo $Id; ?>" name="charity_id">
                    <input type="hidden" id="mode" value="<?php echo strtolower($mode); ?>" name="mode">
                    <div class="col-md-12">
                        <div class="col-md-12 left-right-padding">
                            <div class="portlet light game-info-caption">
                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo caption-width-team">
                                        <i class="icon-settings font-red-sunglo"></i>
                                        <span class="caption-subject bold uppercase"><?php echo $mode;?> Charity Details</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 left-padding">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light ">
                                
                                <div class="portlet-body form">
                                    <div class="form-body top-padding">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="charity_name">Charity
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="charity_name" name="charity_name" placeholder="" value="<?php echo $charity_name; ?>"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="owner">Owner
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="owner" name="owner" placeholder="" value="<?php echo $owner; ?>"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="description">Description
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <textarea type="text" class="form-control input-sm" id="description" name="description" placeholder=""><?php echo $description; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="email" name="email" placeholder="" value="<?php echo $email; ?>"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="mobile_number">Mobile Number
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="mobile_number" name="mobile_number" placeholder="" value="<?php echo $mobile_number; ?>"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address">Address
                                                        <span class="required"> * </span>
                                                    </label>
                                                     <textarea type="text" class="form-control input-sm" id="address" name="address" placeholder=""><?php echo $address; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="city">City
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="city" name="city" placeholder="" value="<?php echo $city; ?>"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="country">Country
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="country" name="country" placeholder="" value="<?php echo $country; ?>"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="zipcode">Zip Code
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="zipcode" name="zipcode" placeholder="" value="<?php echo $zipcode; ?>"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="status">Status
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <select class="form-control input-sm" name="status" id="status">
                                                        <option value="">Select</option>
                                                        <option value="1" <?php echo $status == "1" ? "selected" :"" ?> >Active</option>
                                                        <option value="0" <?php  echo $status == "0" ? "selected" :"" ?>>In-Active</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <?php if($mode=='Edit')
                                                    $btn_text = 'Update';
                                                else
                                                    $btn_text = $mode;
                                                ?>
                                                <input type="submit" name="submit" class="btn btn-info" value="<?php echo $btn_text;?> Details" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include_once('footer.php'); ?>
<script type="text/javascript">
    $('#charity_form').validate({ 
        rules: {        
            charity_name: {
                required: true,
            },
            owner: {
                required: true,
            },
            description: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            mobile_number: {
                required: true,
            },
            address: {
                required: true,
            },
            city: {
                required: true,
            },
            country: {
                required: true,
            },
            zipcode: {
                required: true,
            },
            status: {
                select_status: true,
            },
        },
         messages: {
            charity_name: {
                required: "Please enter charity",
            },
            owner: {
                required: "Please enter owner",
            },
            description: {
                required: "Please enter description",
            },
            email: {
                required: "Please enter email",
                email: "Please enter valid email",
            },
            mobile_number: {
                required: "Please enter mobile number",
            },
            address: {
                required: "Please enter address",
            },
            city: {
                required: "Please enter city",
            },
            country: {
                required: "Please enter country",
            },
            zipcode: {
                required: "Please enter zipcode",
            },
        },
    });

    jQuery.validator.addMethod('select_status', function (value) {
        return (value != '');
    }, "Please select status");
</script>