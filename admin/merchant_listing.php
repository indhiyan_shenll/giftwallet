<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
/****Paging ***/
$Page=1;$RecordsPerPage=25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
$TotalPages=0;
/*End of paging*/

$GiftWalletApi = new Common($dbconn);

$DeleteId="";$msg="";
if(isset($_GET['id']) && !empty($_GET['id'])){
    $TblName = "tbl_merchants";
    $qryParams['merchant_id']=base64_decode($_GET['id']);
    $whereCndn = " WHERE merchant_id=:merchant_id";

    $getDeleteRes = $GiftWalletApi->funExeDeleteRecord($TblName,$qryParams,$whereCndn);

    if ($getDeleteRes==1) {
        header("Location: merchant_listing.php?msg=delete");
        exit;
    }
    $msg = "deleted";
}

if(isset($_GET['msg']) && !empty($_GET['msg'])){
    $msg=$_GET['msg'];
    if ($msg=='delete') {
        $msg_display = 'Merchant details deleted successfully';
    } else if ($msg=='update') {
        $msg_display = 'Merchant details updated successfully';
    }  else if ($msg=='insert') {
        $msg_display = 'Merchant details added successfully';
    }
}

$qrycondition = "";
$filterArr="";
$merchant_name = "";
$outlet_name = "";
$user_email = "";
$user_status = "";
if (isset($_POST['merchant_name']) || isset($_POST['outlet_name']) || isset($_POST['user_email']) || isset($_POST['user_status'])) {
    $filterArr = array();
    if ($_POST['merchant_name']!="" || $_POST['outlet_name']!="" || $_POST['user_email']!="" || $_POST['user_status']!="") {        
        
        if ((strlen($_POST['merchant_name'])) > 0 ) {
            $qrycondition .= " AND merchant_full_name like '%".trim($_POST['merchant_name'])."%'";
            $merchant_name = $_POST['merchant_name'];
        }
        if ((strlen($_POST['outlet_name'])) > 0 ) {
            $qrycondition .= " AND outlet_name like '%".trim($_POST['outlet_name'])."%'";
            $outlet_name = $_POST['outlet_name'];
        }
        if ((strlen($_POST['user_email'])) > 0 ) {
            $qrycondition .= " AND email like '%".trim($_POST['user_email'])."%'";
            $user_email = $_POST['user_email'];
        }
        if ((strlen($_POST['user_status'])) > 0 ) {
            $qrycondition .= " AND status like '%".trim($_POST['user_status'])."%'";
            $user_status = $_POST['user_status'];
        }        
    } else {
        $qrycondition = "";
        $filterArr="";
        $merchant_name = "";
        $outlet_name = "";
        $user_email = "";
        $user_status = "";
    }
} else {
    $qrycondition = "";
    $filterArr="";
    $merchant_name = "";
    $outlet_name = "";
    $user_email = "";
    $user_status = "";
}

include("header.php");
?>
<form name="merchantlist_form" id="merchantlist_form" method="post" action="">
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<!-- overwrtite the css for this particular page -->
<style type="text/css">
    .add_merchant{min-height: 34px !important;}
    .status{font-weight: bolder;}
    .table.dataTable thead:first-child .sorting_asc{background: none !important;}
    table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting{padding: 8px 10px !important;}
    #tbl_merchant_list thead tr th:nth-child(1), th:nth-child(6), th:nth-child(7){
        text-align: center;
        vertical-align: middle;
    }
    #tbl_merchant_list tbody tr td:nth-child(1), td:nth-child(6), td:nth-child(7){
        text-align: center;
        vertical-align: middle;
    }
    #tbl_merchant_list tbody tr td{
        vertical-align: middle;
    }
    .search-orderlist-btns{
        margin-top: 22px;
        padding: 0px;
    }
    .remove-padding{
        padding-left: 0px;
        padding-right: 0px;
    }
    .custombtn {
        width: 95px;
        border-radius: 6px !important;
        box-shadow: 0px 3px 1px #888888 !important;
    }
    .table-scrollable {
        margin-top: 20px !important;
    }
    .btn-group{
        position: absolute;
        display: block;
        vertical-align: middle;
        margin-top: -10px;
    }
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <?php if ($msg!="") { ?>
        <div class="custom-alerts alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><?php echo $msg_display; ?></div>
    <?php } ?>
    <div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light">
                <div class="portlet-title" style="margin-bottom: 0px;">

                    <div class="caption font-dark">
                        <i class="icon-users font-dark"></i>
                        <span class="caption-subject bold uppercase">Merchants</span>
                    </div>
                    <div class="tools">
                        <a href="manage_merchants.php" class="btn btn-circle btn-outline red dropdown-toggle add_merchant">
                            Add Merchant&nbsp;
                        </a>
                    </div>
                </div>

                <div class="portlet-body flip-scroll">
                   <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch">
                            <div class="col-md-9 col-sm-9 col-xs-12 remove-padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <label>Merchant:</label>
                                    <input type="text" name="merchant_name" id="merchant_name" class="form-control form-control-inline" placeholder="Merchant" value="<?php echo $merchant_name; ?>">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <label>Outlet:</label>
                                    <input type="text" name="outlet_name" id="outlet_name" class="form-control form-control-inline" placeholder="Outlet" value="<?php echo $outlet_name; ?>">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <label>Email:</label>
                                    <input type="text" name="user_email" id="user_email" class="form-control form-control-inline" placeholder="Email" value="<?php echo $user_email; ?>">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <label>Status:</label>
                                    <select name="user_status" id="user_status" class="form-control form-control-inline">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo (isset($user_status) && $user_status == '1') ? 'selected' : ''; ?>>Active</option>
                                        <option value="0" <?php echo (isset($user_status) && $user_status == '0') ? 'selected' : ''; ?>>In-Active</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 remove-padding">
                                <div class="col-md-12 col-sm-12 col-xs-12 search-orderlist-btns">
                                    <button type="button" class="btn yellow custombtn" id="search"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
                                    <a type="button"  href="merchant_listing.php" class="btn red custombtn"><i class="fa fa-times-circle" aria-hidden="true"></i> Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped table-condensed flip-content" id="tbl_merchant_list">
                        <thead class="flip-content">
                            <tr>
                                <th width="5%">S.No</th>
                                <th nowrap>Merchant</th>
                                <th nowrap>Outlet</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                                $Qry="SELECT * FROM tbl_merchants where merchant_id <>'' $qrycondition order by merchant_id desc";
                                if ($filterArr!="") {
                                    $qryParams = $filterArr;
                                    $getResCnt = $GiftWalletApi->funBckendExeSelectQuery($Qry,$qryParams);
                                }
                                else
                                    $getResCnt = $GiftWalletApi->funBckendExeSelectQuery($Qry);
                                    // print_r($getResCnt);
                                if (count($getResCnt,COUNT_RECURSIVE)>1) {
                                    $TotalPages=ceil(count($getResCnt)/$RecordsPerPage);
                                    $Start=($Page-1)*$RecordsPerPage;
                                    $sno=$Start+1;
                                    $Qry.=" limit $Start,$RecordsPerPage";
                               	    $getMerchant = $GiftWalletApi->funBckendExeSelectQuery($Qry);

							   	    if (count($getMerchant)>0) {
							   		    foreach ($getMerchant as $MerchantListData) {
                                        $statusColor=($MerchantListData["status"]=="1")?"#659be0":"#9c3b3b";
                                        $status=($MerchantListData["status"]=="1")?"Active":"In-Active";
							?>
						       <tr>
						       		<td><?php echo $sno;?></td>
						       		<td><?php echo $MerchantListData["merchant_full_name"];?></td>
                                    <td><?php echo $MerchantListData["outlet_name"];?></td>
						       		<td><?php echo $MerchantListData["email"];?></td>
						       		<td><?php echo $MerchantListData["mobile_number"];?></td>						       		
						       		<td class="status"><span class="label label-info" style="background-color:<?php echo $statusColor?>"><?php echo $status;?></span></td>
						       		<td>
						       			<div class="actions">
										    <div class="btn-group">
										        <a class="btn btn-xs default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Actions
										            <i class="fa fa-angle-down"></i>
										        </a>
										        <ul class="dropdown-menu pull-right">
										            <li>
										                <a href="manage_merchants.php?id=<?php echo base64_encode($MerchantListData["merchant_id"]);?>">Edit Details</a>
										            </li>
                                                    <li>
                                                        <a href="merchant_listing.php?id=<?php echo base64_encode($MerchantListData["merchant_id"]);?>">Remove Details</a>
                                                    </li>
										        </ul>
										    </div>
										</div>
									</td>
						       </tr>
                            <?php   
							    $sno++;
						            }
								} else {
                                    echo "<tr><td colspan='10' style='text-align:center;'>No record(s) found </td></tr>";
								}
                            } else {
                                echo "<tr><td colspan='10' style='text-align:center;'>No record(s) found </td></tr>";
                            }

                            ?>
                            
                        </tbody>
                    </table>
                </div>
                <div>
                    <?php
                        if($TotalPages > 1){

                            echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
                            $FormName = "merchantlist_form";
                            require_once ("paging.php");
                            echo "</td></tr>";
                        }
                    ?>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
</div>
</form>
<?php include_once("footer.php"); ?>
<script>
$('#tbl_merchant_list').DataTable( {
    "bPaginate": false,
    "bFilter": false,
    "bInfo": false,
    "iDisplayLength": false ,
   'aoColumnDefs': [
        {'aTargets': [9],'bSortable': false,
        'aTargets': [0, 5, 6],'bSortable': false,},
    ],
     "order": [[ 0, "asc" ]]  
} );
$("#search").click(function() {
    var merchant_name = $('#merchant_name').val();
    var outlet_name = $('#outlet_name').val();
    var user_email = $('#user_email').val();
    var user_status = $('#user_status').val();
    var filterArr = [];
    filterArr['merchant_name']=merchant_name;
    filterArr['outlet_name']=outlet_name;
    filterArr['user_email']=user_email;
    filterArr['user_status']=user_status;
    if (filterArr['merchant_name']=="" && filterArr['outlet_name']=="" && filterArr['user_email']=="" && filterArr['user_status']=="") {
        // $('#giftlist_form').submit();
    } else {
        $('#merchantlist_form').submit();
    }
}); 
</script>