Login API
---------
http://localhost/indianfood/api/?type=login
email:admin@gmail.com
password:123456
userType:vendor
deviceToken:testss
userPlatform:tests

Register API
------------
http://demo.shenll.net/indianfood/api_v1/api.php?type=register
fullName: sts
email: test@gmail.com
password: 123456
userType: vendor
address: test address
country: india
zipCode: 354637
mobileNumber: 78474774747
deviceToken: 643834834834346384363
userPlatform: IOS

Forgetpassword API
------------------
http://demo.shenll.net/indianfood/api_v1/api.php?type=forgetpassword
email: admin@gmail.com
userType: vendor

Vendorlist API
--------------
http://demo.shenll.net/indianfood/api_v1/api.php?type=vendorlist

Myorder API
-----------
http://demo.shenll.net/indianfood/api_v1/api.php?type=myorder
userId: 123213

OrderHistory API
----------------
http://demo.shenll.net/indianfood/api_v1/api.php?type=orderhistory
userId: 123213

UpdateOrder API
---------------
http://demo.shenll.net/indianfood/api_v1/api.php?type=updateorder
userId: 123213
orderId: 123123
orderStatus:pay

CreateUpdateCategory API
------------------------
http://demo.shenll.net/indianfood/api_v1/api.php?type=createupdatecategory
userId: 123213
categoryId : 123
categoryOrder:2
vendorId:12
categoryName:Juice
description:test description
image: imageurl

DeleteCategory API
------------------
http://demo.shenll.net/indianfood/api_v1/api.php?type=deletecategory
userId: 123213
categoryId : 123

DeleteItem API
------------------
http://demo.shenll.net/indianfood/api_v1/api.php?type=deleteitem
userId: 123213
itemId : 123

CreateUpdateItem API
--------------------

http://demo.shenll.net/indianfood/api_v1/api.php?type=createupdateitem
userId: 123213
itemId : 123
itemOrder:12
itemName : Maindish
itemDesc : 123
itemPrice : Maindish
itemType : veg/Non-veg
categotyId: 123
image: imageurl

UpdateOrderStatus API
---------------------
http://demo.shenll.net/indianfood/api_v1/api.php?type=updateorderstatus

orderId: 1
userId :12312
orderStatus : cancel
reason: no time


CustomerDetails API
-------------------
http://demo.shenll.net/indianfood/api_v1/api.php?type=customerdetails
userId: 123213

getreport API
------------
http://demo.shenll.net/indianfood/api_v1/api.php?type=getreport
userId: 123213
startDate:2017-10-05
endDate:2017-10-05

http://demo.shenll.net/indianfood/api_v1/api.php?type=getinvoice
userId: 123213

http://demo.shenll.net/indianfood/api_v1/api.php?type=updateinvoice
userId :12312
invoiceId : 123
invoiceStatus :paid/Pending

http://demo.shenll.net/indianfood/api_v1/api.php?type=updaterating
userId :12312
orderId : 123
vendorId: 344
rate: 4
http://demo.shenll.net/indianfood/api_v1/api.php?type=foodtype

http://demo.shenll.net/indianfood/api_v1/api.php?type=categorylist

http://demo.shenll.net/indianfood/api_v1/api.php?type=confirmorder&confirmJson= "{"vendorId": "1","userId": "12312","price": "123","startDate": "2017-10-05","endDate": "2017-10-05","orderTime": "12:00","vendorname": "dfa","ratings": "5","orderStatus": "pay","ordersList": [{
"id": "12323","name": "egg","type": "veg"}, {"id": "12323","name": "egg","type": "veg"}]}"

http://demo.shenll.net/indianfood/api_v1/api.php?type=addpackageedit&packageJson="{"packageId": "123","packageName": "rice","price": "10","dishlist": [{"10": "1,2","11": "3,4" }], "image": "","type": "veg/Nonveg/Halal","days": "5"}"

http://demo.shenll.net/indianfood/api_v1/api.php?type=updateprofile
userId:123
name: sts
mobile: 1231231232
address: address
country: india
Zip: 3444
email : asd@test.com
password: 123213
type :customer
documentUpload:
deviceToken : dfadfa
deviceInfo: daf 