<?php
header('Content-Type: application/json');

include_once('../includes/configure.php');
include_once('../includes/PHPmailer/PHPMailerAutoload.php');
include_once('common.php');
include_once('api.php');

// Initialize FoodAppApi
$foodAppApi = new FoodAppApi($dbconn);

$apiType = isset($_GET["type"]) ? strtolower(trim($_GET["type"])) : "" ;
$reqMethod = $_SERVER['REQUEST_METHOD'];
$errorReqMethdMsg = array("error" => "Check request method!!");

$apiRouteConfig = json_decode($foodAppApi->apiRouteConfig(), true);
// Check request method and configure API method's are same
// if (!empty($apiType) && array_key_exists($apiType, $apiRouteConfig)) {
if (!empty($apiType)) {
    switch (true) {
    	case ($apiType == "register" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->register($_REQUEST);
            break; 
        case ($apiType == "login" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->login($_REQUEST);
            break;         
        case ($apiType == "forgetpassword" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->forgetPassword($_REQUEST);
            break; 
        case ($apiType == "creategift" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->createGift($_REQUEST);
            break; 
        case ($apiType == "getcharitylist" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->getCharityList($_REQUEST);
            break;
        case ($apiType == "updateprofile" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->updateProfile($_REQUEST);
            break;
        default:
            echo $foodAppApi->jsonResponse($errorReqMethdMsg);
    }
} else {
    echo $foodAppApi->jsonResponse(array("error" => "Api doesn't exist!"));
    exit;
}
