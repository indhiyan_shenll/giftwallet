<?php
class FoodAppApi extends Common {

	public $dbconn;
	public $currentDate;
	public function __construct(PDO $dbconn){
		$this->dbconn = $dbconn;
		$this->currentDate = date("Y-m-d h:i:s");
	}
	public function apiRouteConfig() {
		$apiReqMethod = array ( 
							"register" => "POST",
							"login" => "POST",							
							"forgetpassword" => "POST",								
							"creategift" => "POST",						
							"getcharitylist" => "GET",
							"updateprofile" => "POST"
						);
		return $this->jsonResponse($apiReqMethod);
	}

	// Register vendor/customer
	public function register($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array("firstName", "lastName", "email", "password", "confirmPassword");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);			
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}

			$currentDate = $this->currentDate;
			// Check vendor/customer already exist
			$selQryParams = array (  ":email" => $request["email"] );
		    $whereCondtn = $this->funParseQryParams($selQryParams, "user_type", "AND");   
		    $reqQryParams = array (
								"fetchType" => "singleRow",
								"selectField" => "count(email) as countRows",
								"tableName" => "tbl_users",
								"whereCondition" => $whereCondtn
							);
			$chekEmailExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);
			$responseDate = array();
			if (isset($chekEmailExistRes["countRows"]) && $chekEmailExistRes["countRows"] > 0) {
				$responseDate["status"] = 0;
				$responseDate["message"] = "Email already exists!";
				$responseDate["userid"] = "";
			} else {				 
				// Register vendor/customer
				$insQryParams = array ( 
									":first_name" => $request["firstName"],
									":last_name" => $request["lastName"],
									":email" => $request["email"],
									":mobile_number" => $request["mobileNumber"],									
									":address" => $request["address"],
									":city" => $request["city"],
									":country" => $request["country"],
									":zip_code" => $request["zipCode"],
									":password" => $request["password"],
									":confirm_password" => $request["confirmPassword"],
									":status" => 1,
									":device_token" => $request["deviceToken"],
									":user_platform" => $request["userPlatform"],
									":created_date" => $currentDate,
									":modified_date" => $currentDate
								);
				$insQryResponse = $this->funExeInsertRecord("tbl_users", $insQryParams);
				if (!empty($insQryResponse))	 {
					$selInsQryParams = array(":email" => $request["email"] );
				    // Select last inserted vendor/customer
				    $selInsWhereCondtn = $this->funParseQryParams($selInsQryParams);
				    $reqQryParams = array (
										"fetchType" => "singleRow",
										"selectField" => "*",
										"tableName" => "tbl_users",
										"whereCondition" => $selInsWhereCondtn
									);
					$lastRegisterdUser = $this->funExeSelectQuery($reqQryParams, $selInsQryParams);					
					// Send email to Admin
					$mailParams = array(
									"fromAddress" => "gift@giftwallet.com",
			                        "toAddress" => "indhiyan.shenll@gmail.com",
			                        "customerName" => "Indhiyan",
			                        "subject" => "User registration",
			                        "bodyMsg" => "<p>Dear Admin</p>
			                                        <p>Congratulations! The following person has just downloaded your food App and registered as a new customer!</p>
			                                        <p>Username: ".$lastRegisterdUser["first_name"]." ".$lastRegisterdUser["last_name"]."</p>
			                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
			                                        <p style='line-height:5px;'>Best Regards,</p> 
			                                        <p style='line-height:0px;'>Giftwallet support Team</p>"
			                    );
					$sendEmail = $this->sendEmailNotification($mailParams);
					if ($sendEmail) {
						$responseDate["status"] = 1;
						$responseDate["message"] = "User registered successfully";
						$responseDate["userid"] = !empty($lastRegisterdUser["user_id"]) ? $lastRegisterdUser["user_id"] : "" ;
					}
				} else {
					$responseDate["status"] = 0;
					$responseDate["message"] = "Something Issue in Registration!";
					$responseDate["userid"] = "";
				}
			}			
			return $this->jsonResponse($responseDate);
		}
	}

	// Login
	public function login($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array('email', 'password');
			$errors = $this->funCheckRequiredFields($request, $requiredFields);			
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}			
			// Select User
			$currentDate = $this->currentDate;
			$selQryParams = array (	
								":email" => $request["email"],
								":password" => $request["password"],
							);
		    $whereCondtn = $this->funParseQryParams($selQryParams, "password", "AND");
		    $reqQryParams = array (
								"fetchType" => "singleRow",
								"selectField" => "",
								"tableName" => "tbl_users",
								"whereCondition" => $whereCondtn
							);
			$selQryResponse = $this->funExeSelectQuery($reqQryParams, $selQryParams);			
			$responseDate = array();
			if (count($selQryResponse) > 0) {
				$responseDate["status"] = 1;
				$responseDate["message"] = "Logged successfully!";
				$responseDate["userid"] = $selQryResponse["user_id"];

				// Update device info
				$updateQryParams = array ( 
										":device_token" => $request["deviceToken"], 
										":user_platform" => $request["userPlatform"],
										":modified_date" => $currentDate,
									);
				$setCondtn = $this->funParseQryParams($updateQryParams, "modified_date", ",");
				$updateQryParams[":user_id"] = $selQryResponse["user_id"];
			    $reqQryParams = array (
									"tableName" => "tbl_users",
									"setCondtn" =>$setCondtn,
									"whereCondition" => "user_id=:user_id"
								);
				$updateQryResponse = $this->funExeUpdateRecord($reqQryParams, $updateQryParams);
			} else {
				$responseDate["status"] = 0;
				$responseDate["message"] = "User doesn't exists! OR email and password doesn't match";
				$responseDate["userid"] = "";
			}
			return $this->jsonResponse($responseDate);
		}		
	}	

	// ForgetPassword
	public function forgetPassword($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array('email');
			$errors = $this->funCheckRequiredFields($request, $requiredFields);			
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}			
			// Check vendor/customer already exist
			$currentDate = $this->currentDate;
			$selQryParams = array (  ":email" => $request["email"] );
		    $whereCondtn = $this->funParseQryParams($selQryParams); 
		    $reqQryParams = array (
								"fetchType" => "singleRow",
								"selectField" => "*",
								"tableName" => "tbl_users",
								"whereCondition" => $whereCondtn
							);
			$userInfo = $this->funExeSelectQuery($reqQryParams, $selQryParams); 
			$responseDate = array();
			if (is_array($userInfo) && count($userInfo) > 0) {

				// Send email to Customer
				$mailParams = array(
								"fromAddress" => "gift@giftwallet.com",
		                        "toAddress" => "indhiyan.shenll@gmail.com",
		                        "customerName" => "Indhiyan",
		                        "subject" => "Forget password",
		                        "bodyMsg" => "<p>Dear ".$userInfo["first_name"]." ".$userInfo["last_name"]."</p>
		                                        <p>Password for your account is: ".$userInfo["password"]."</p>
		                                        <p>Username: Indhiyan</p>
		                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
		                                        <p style='line-height:5px;'>Best Regards,</p> 
		                                        <p style='line-height:0px;'>KazaFood support Team</p>"
		                    );
				$sendEmail = $this->sendEmailNotification($mailParams);
				if ($sendEmail) {
					$responseDate["status"] = 1;
					$responseDate["message"] = "Email has been sent to your email with passowrd";
				}
			} else {				 
				$responseDate["status"] = 0;
				$responseDate["message"] = "Email not exists!";
			}			
			return $this->jsonResponse($responseDate);
		}
	}	

	// Fetch list of orders that customer ordered till now
	public function createGift($request) {

		if (is_array($request) && count($request) > 0) {

			// Check required fields
			$requiredFields = array("userId", "giftName", "email", "mobileNumber", "message");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);			
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}

			// Select inviting user exists ?
			$currentDate = $this->currentDate;
			$invUserParams = array( ":email" => $request["email"] );
			$selInvUserWhereCondtn = $this->funParseQryParams($invUserParams);
			$reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "*",
					"tableName" => "tbl_users",
					"whereCondition" => $selInvUserWhereCondtn
				);
			$invitingUser = $this->funExeSelectQuery($reqQryParams, $invUserParams);
			$invitingUserId = "";
			if (count($invitingUser) == 0) {
				$randomPassword = $this->generateRandAlphanumeric();
				$insQryParams = array ( 
									":first_name" => $request["firstName"],
									":last_name" => $request["lastName"],
									":email" => $request["email"],
									":mobile_number" => $request["mobileNumber"],
									":password" => $randomPassword,
									":confirm_password" => $randomPassword,
									":status" => 1,
									":created_date" => $currentDate,
									":modified_date" => $currentDate
								);
				$insQryResponse = $this->funExeInsertRecord("tbl_users", $insQryParams);
				$invitingUserId = $insQryResponse;
			}
			$randomInviteCode = $this->generateRandAlphanumeric();
			$insGiftQryParams = array ( 
									":invite_name" => $request["giftName"],
									":invite_reg_date" => $currentDate,
									":admin_id" => $request["userId"],
									":invite_user_id" => !empty($invitingUserId) ? $invitingUserId : $invitingUser["user_id"],
									":code" => $randomInviteCode,
									":message" => $request["message"],
									":created_date" => $currentDate
								);
			$insGiftResponse = $this->funExeInsertRecord("tbl_gift_invitation", $insGiftQryParams);
			if (!empty($insGiftResponse)) {
				$responseDate["status"] = 1;
				$responseDate["message"] = "Gift created successfully";
			} else {
				$responseDate["status"] = 0;
				$responseDate["message"] = "Somthing issue in creating gift!";
			}
			
			return $this->jsonResponse($responseDate);
		}
	}

	// Fetch list of orders that customer ordered till now
	public function getCharityList($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {

			// Check active charity exisits ?
			$selQryParams = array ( ":status" => 1);
		    $whereCondtn = $this->funParseQryParams($selQryParams);
		    $reqQryParams = array (
				"fetchType" => "singleRow",
				"selectField" => "count(charity_id) as countRows",
				"tableName" => "tbl_charity",
				"whereCondition" => $whereCondtn
			);					    

			$chekCharityExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);
			$responseDate = array();
			if (isset($chekCharityExistRes["countRows"]) && $chekCharityExistRes["countRows"] > 0) {

			    $reqQryParams = array (
					"fetchType" => "multipleRow",
					"selectField" => "",
					"tableName" => "tbl_charity",
					"whereCondition" => $whereCondtn
				);
				$charityResponse = $this->funExeSelectQuery($reqQryParams, $selQryParams);
				$charityListArr = array();
				if (!empty($charityResponse)) {
					$i = 0;
					foreach ($charityResponse as $charity) {
						$charityListArr[$i]["charityId"] = $charity["charity_id"];
						$charityListArr[$i]["charityName"] = $charity["charity_name"];
						// $charityListArr[$i]["description"] = $charity["description"];
						// $charityListArr[$i]["email"] = $charity["email"];
						// $charityListArr[$i]["mobile_number"] = $charity["mobile_number"];
						$charityListArr[$i]["status"] = $charity["status"];
						$i++;
					}
				}
				$responseDate["status"] = 1;
				$responseDate["message"] = "success";
				$responseDate["orders"] = $charityListArr;
			} else {				 
				$responseDate["status"] = 1;
				$responseDate["message"] = "No charities found!";
			}			
			return $this->jsonResponse($responseDate);
		}
	}	

	// Update invoice
	public function updateProfile($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array("userId");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			$currentDate = $this->currentDate;
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			// Check vendor/customer is exist
			$selQryParams = array (  ":user_id" => $request["userId"] );
		    $whereCondtn = $this->funParseQryParams($selQryParams);   
		    $reqQryParams = array (
				"fetchType" => "singleRow",
				"selectField" => "count(user_id) as countRows",
				"tableName" => "tbl_users",
				"whereCondition" => $whereCondtn
			);
			$chekUserExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);
			$responseDate = array();
			if (isset($chekUserExistRes["countRows"]) && $chekUserExistRes["countRows"] > 0) {

				// $userImg = $request["userImg"];
				// $uploadImg = "";
				// if (!empty($userImg)) {
				// 	// Convert base64image to image
				// 	$uploadImg = $this->base64toImage($userImg);
				// }
				$updateQryParams = array ( 
									":first_name" => $request["firstName"],
									":last_name" => $request["lastName"],
									":email" => $request["email"],
									":mobile_number" => $request["mobileNumber"],									
									":address" => $request["address"],
									":city" => $request["city"],
									":country" => $request["country"],
									":zip_code" => $request["zipCode"],
									":password" => $request["password"],
									":confirm_password" => $request["confirmPassword"],
									":status" => 1,
									":device_token" => $request["deviceToken"],
									":user_platform" => $request["userPlatform"],
									":modified_date" => $currentDate
								);

				$setCondtn = $this->funParseQryParams($updateQryParams, "modified_date", ",");
			    $reqQryParams = array (
						"tableName" => "tbl_users",
						"setCondtn" =>$setCondtn,
						"whereCondition" => $whereCondtn
					);
				$updateQryResponse = $this->funExeUpdateRecord($reqQryParams, array_merge($updateQryParams, $selQryParams));
				// $reqQryParams = array (
				// 	"fetchType" => "singleRow",
				// 	"selectField" => "*",
				// 	"tableName" => "tbl_users",
				// 	"whereCondition" => "user_id=:user_id"
				// );
				// unset($selQryParams[':user_type']);
				// $userInfo = $this->funExeSelectQuery($reqQryParams, $selQryParams);
				$responseDate["status"] = 1;
				$responseDate["message"] = "User updated successfully";

			} else {				 
				$responseDate["status"] = 0;
				$responseDate["message"] = "User not exists";
			}			
			return $this->jsonResponse($responseDate);
		}
	}	
}