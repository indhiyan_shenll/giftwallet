<?php
include('common.php');
class givengiftapp extends common {
	// public $dbconn;
	// public function __construct(PDO $dbconn){
	// 	$this->dbconn = $dbconn;
	// }
	public function funLoginDetails($email,$password) {
		if (!empty($email) && !empty($password)) { 
			$ArrLoginDetails["status"]="0";
			$ArrLoginDetails["message"]="success";
			$ArrLoginDetails["userId"]="123";
            $ArrLoginDetails["name"]="sts";
            $ArrLoginDetails["mobile"]="1231231232";
            $ArrLoginDetails["address"]="No 24,Anna salai chennai";
            $ArrLoginDetails["country"]="india";
            $ArrLoginDetails["zip"]="34443";
            $ArrLoginDetails["email"]="test@gmail.com";
            $ArrLoginDetails["amount"]="12321";
			return $this->funjsonReturn($ArrLoginDetails);
	    } else {
			$ArrLoginDetails["status"]="1";
            $ArrLoginDetails["message"]="failure";
            return $this->funjsonReturn($ArrLoginDetails);
	    }
	}
	public function funRegisterDetails($name,$mobile,$address,$country,$zip,$email,$password,$deviceToken,$deviceInfo) {
		if (!empty($name) && !empty($mobile) && !empty($address) && !empty($country) && !empty($zip) && !empty($email) && !empty($password) && !empty($deviceToken) && !empty($deviceInfo) ) { 
			$ArrRegisterDetails["status"]="0";
			$ArrRegisterDetails["message"]="success";
            $ArrRegisterDetails["userId"]="122";
			return $this->funjsonReturn($ArrRegisterDetails);
	    } else {
			$ArrRegisterDetails["status"]="1";
            $ArrRegisterDetails["message"]="failure";
            return $this->funjsonReturn($ArrRegisterDetails);
	    }
	}

	public function funForgetPasswordDetails($email) {
        if (!empty($email)) { 
        	$ArrForgetPasswordDetails["status"]="0";
			$ArrForgetPasswordDetails["message"]="success";
			return $this->funjsonReturn($ArrForgetPasswordDetails);
		} else {
            $ArrForgetPasswordDetails["status"]="1";
            $ArrForgetPasswordDetails["message"]="failure";
            return $this->funjsonReturn($ArrForgetPasswordDetails);
		}

	}
    public function funGiveGiftDetails($senderName,$senderEmail,$senderPhone,$giftCode,$amount,$paypalId) {
		if (!empty($senderName) && !empty($senderEmail) && !empty($senderPhone) && !empty($giftCode) && !empty($amount) && !empty($paypalId)) { 
			$ArrGiveGiftDetails["status"]="0";
            $ArrGiveGiftDetails["message"]="success";
            return $this->funjsonReturn($ArrGiveGiftDetails);
		} else {
			$ArrGiveGiftDetails["status"]="1";
            $ArrGiveGiftDetails["message"]="failure";
            return $this->funjsonReturn($ArrGiveGiftDetails);
		}

	}

	public function funGetCharityDetails($userid) {

		if (!empty($userid)) { 

			$ArrGetCharityDetails["status"]="0";
			$ArrGetCharityDetails["message"]="success";
			$ArrGetCharityDetails["charity"]=array( 
				array (
                 	"charityId"=> "1",
				    "charityName"=> "dfa",
                 ),
				array (
	                "charityId"=> "2",
					"charityName"=> "dfx",
                ),
            );
          return $this->funjsonReturn($ArrGetCharityDetails);
		} else {

			$ArrGetCharityDetails["status"]="1";
            $ArrGetCharityDetails["message"]="failure";
            return $this->funjsonReturn($ArrGetCharityDetails);
        }

	}

	public function funSendMoneyforMerchant($userId,$merchantId,$amount,$message) {
        if (!empty($userId) &&  !empty($merchantId) && !empty($amount) && !empty($message) ) {

        	$ArrSendMoneyforMerchant["status"]="0";
            $ArrSendMoneyforMerchant["message"]="success";
            return $this->funjsonReturn($ArrSendMoneyforMerchant);
        }
        else {
            $ArrSendMoneyforMerchant["status"]="1";
            $ArrSendMoneyforMerchant["message"]="failure";
            return $this->funjsonReturn($ArrSendMoneyforMerchant);
        }
	}
	public function funGetGiftListDetails($userid) {
		if (!empty($userid)) { 
        	$ArrGetGiftListDetails["status"]="0";
			$ArrGetGiftListDetails["message"]="success";
			$ArrGetGiftListDetails["gifts"][]=array(
				"name"=>"Gift 1",
				"giftId"=>"1",
				"amount"=>"10",
				"giftStatus"=>"0",
				"giftCreatedOn"=>"2017-10-10  17:10",
                "userId"=>"123213",
                "giftCode"=>"tUv12Z",
					"members"=>array( 
						array (
		                "name"=>"Mano",
                        "memberId"=>"1",
                        "inviteStatus"=>"0",
                        "amount"=>"10",
                        "paidOnDate"=>"2017-10-01 17:30",
		                 ),
						array (
		                "name"=>"Muthu",
                        "memberId"=>"2",
                        "inviteStatus"=>"1",
                        "paidOnDate"=>"2017-10-01 17:30",
                        "amount"=>"10",
		                ),
                	),
                );
			
			return $this->funjsonReturn($ArrGetGiftListDetails);
		} else {
            $ArrGetGiftListDetails["status"]="1";
            $ArrGetGiftListDetails["message"]="failure";
            return $this->funjsonReturn($ArrGetGiftListDetails);
		}
	}
    public function funDeleteGiftDetails($userid,$giftid) {
        if (!empty($userid) && !empty($giftid)) {
        	$ArrDeleteGiftDetails["status"]="0";
			$ArrDeleteGiftDetails["message"]="success";
			return $this->funjsonReturn($ArrDeleteGiftDetails);
        } else {
           $ArrDeleteGiftDetails["status"]="1";
           $ArrDeleteGiftDetails["message"]="failure";
           return $this->funjsonReturn($ArrDeleteGiftDetails);
        }
    }
	public function funPayForGiftDetails($userid,$giftid,$amount,$paypalid) {
		if (!empty($userid) && !empty($giftid) && !empty($amount) && !empty($paypalid)) {
        	$ArrPayForGiftDetails["status"]="0";
			$ArrPayForGiftDetails["message"]="success";
			return $this->funjsonReturn($ArrPayForGiftDetails);
        } else {
           $ArrPayForGiftDetails["status"]="1";
           $ArrPayForGiftDetails["message"]="failure";
           return $this->funjsonReturn($ArrPayForGiftDetails);
        }
	}
    public function funTransactionsHistory($userid,$fromdate,$todate) {
    	if (!empty($userid) && !empty($fromdate) && !empty($todate)) { 
    		$ArrTransactionsHistory["status"]="0";
			$ArrTransactionsHistory["message"]="success";
			$ArrTransactionsHistory["transactions"]=array( 
				array (
             	   "transactionId"=>"101",
				   "transactionDate"=>"2017-10-01 17:20",
				   "transactionFor"=>"John's Birthday",
				   "amount"=>"10",
				   "status"=>"1",
				   "message"=>"Spent for John's Birthday",
				   "userId"=>"121331",
                 ),
			);
          return $this->funjsonReturn($ArrTransactionsHistory);
    	} else {
			$ArrTransactionsHistory["status"]="1";
            $ArrTransactionsHistory["message"]="failure";
            return $this->funjsonReturn($ArrTransactionsHistory);
        }
	}
	public function funUpdateProfileDetails($userid,$name,$mobile,$address,$country,$zip,$email,$password) {

		if (!empty($userid) && !empty($name) && !empty($mobile) && !empty($address) && !empty($country) && !empty($zip) && !empty($email) && !empty($password)) {
        	$ArrUpdateProfileDetails["status"]="0";
			$ArrUpdateProfileDetails["message"]="success";
			return $this->funjsonReturn($ArrUpdateProfileDetails);
        } else {
           $ArrUpdateProfileDetails["status"]="1";
           $ArrUpdateProfileDetails["message"]="failure";
           return $this->funjsonReturn($ArrUpdateProfileDetails);
        }

	}

	public function  funGetReceivedList($userid) {
		if (!empty($userid)) { 
    		$ArrGetReceivedList["status"]="0";
			$ArrGetReceivedList["message"]="success";
			$ArrGetReceivedList["transactions"]=array( 
				array (
             	   "transactionId"=>"101",
				   "transactionDate"=>"2017-10-01 17:20",
				   "transactionFor"=>"John's Birthday",
				   "amount"=>"10",
				   "status"=>"1",
				   "message"=>"Spent for John's Birthday",
				   "userId"=>"121331",
                 ),
			);
          return $this->funjsonReturn($ArrGetReceivedList);
    	} else {
			$ArrGetReceivedList["status"]="1";
            $ArrGetReceivedList["message"]="failure";
            return $this->funjsonReturn($ArrGetReceivedList);
        }
	}
	public function funSendMoneyForCharity($jsonParams) {
		if (!empty($jsonParams)) { 
			$ArrSendMoneyForCharity["status"]="0";
			$ArrSendMoneyForCharity["message"]="success";
			return $this->funjsonReturn($ArrSendMoneyForCharity);
		} else {
            $ArrSendMoneyForCharity["status"]="1";
            $ArrSendMoneyForCharity["message"]="failure";
            return $this->funjsonReturn($ArrSendMoneyForCharity);
		}

	}
	public function funCreateGift($jsonParams) {
		if (!empty($jsonParams)) { 
			$ArrCreateGift["status"]="0";
			$ArrCreateGift["message"]="success";
			return $this->funjsonReturn($ArrCreateGift);
		} else {
            $ArrCreateGift["status"]="1";
            $ArrCreateGift["message"]="failure";
            return $this->funjsonReturn($ArrCreateGift);
		}

	}

	public function funUpdateGift($jsonParams) {
		if (!empty($jsonParams)) { 
			$ArrUpdateGift["status"]="0";
			$ArrUpdateGift["message"]="success";
			return $this->funjsonReturn($ArrUpdateGift);
		} else {
            $ArrUpdateGift["status"]="1";
            $ArrUpdateGift["message"]="failure";
            return $this->funjsonReturn($ArrUpdateGift);
		}

	}

	
}
?>