<?php
    // include('../includes/configure.php');
    header('Content-Type: application/json');
	require_once('givengiftapp.php');
	// $returnGivenGiftObject = new indianfoodapp($dbconn);
	$returnGivenGiftObject = new givengiftapp();
	// Declaring API HTTP request type
	$apiRequestMethod = array(
		"login" => "POST",
		"register"=>"POST",
		"forgetpassword"=>"POST",
		"givegift"=>"POST",
		"getcharity"=>"POST",
		"sendmoneyformerchant"=>"POST",
		"getgiftlist"=>"POST",
		"deletegift"=>"POST",
		"payforgift"=>"POST",
		"transactionshistory"=>"POST",
		"updateprofile"=>"POST",
		"getreceivedlist"=>"POST",
		"sendmoneyforcharity"=>"GET",
		"creategift"=>"GET",
		"updategift" =>"GET"
	);

	$errorReqTypeMsg = array("status" => "Please check request method!!");
	$requestMethod = $_SERVER['REQUEST_METHOD'];
    if ( isset($_GET["type"]) && trim($_GET["type"])!="") {
    	$gettype =   stripslashes($_GET["type"]);

		if (strtolower($gettype)=="login" && $requestMethod == $apiRequestMethod["login"] ) {
			$email    =  stripslashes($_REQUEST["email"]);
	        $password =  stripslashes($_REQUEST["password"]);
			echo $returnGivenGiftObject->funLoginDetails($email,$password); 
	        
        } elseif ( strtolower($gettype)=="register" && $requestMethod == $apiRequestMethod["register"] ) {    
			$name       =  stripslashes($_REQUEST["name"]);
	        $mobile     =  stripslashes($_REQUEST["mobile"]);
	        $address    =  stripslashes($_REQUEST["address"]);
	        $country    =  stripslashes($_REQUEST["country"]);
	        $zip        =  stripslashes($_REQUEST["zip"]);
	        $email      =  stripslashes($_REQUEST["email"]);
	        $password   =  stripslashes($_REQUEST["password"]);
            $deviceToken=  stripslashes($_REQUEST["deviceToken"]);
            $deviceInfo =  stripslashes($_REQUEST["deviceInfo"]);
			echo $returnGivenGiftObject->funRegisterDetails($name,$mobile,$address,$country,$zip,$email,$password,$deviceToken,$deviceInfo);
		} elseif ( strtolower($gettype)=="forgetpassword" && $requestMethod == $apiRequestMethod["forgetpassword"] ) {    
	        $email =  stripslashes($_REQUEST["email"]);
            echo $returnGivenGiftObject->funForgetPasswordDetails($email); 
        } elseif ( strtolower($gettype)=="givegift" && $requestMethod == $apiRequestMethod["givegift"] ) {    
	        $senderName =  stripslashes($_REQUEST["senderName"]);
	        $senderEmail =  stripslashes($_REQUEST["senderEmail"]);
	        $senderPhone =  stripslashes($_REQUEST["senderPhone"]);
	        $giftCode =  stripslashes($_REQUEST["giftCode"]);
	        $amount =  stripslashes($_REQUEST["amount"]);
	        $paypalId =  stripslashes($_REQUEST["paypalId"]);
            echo $returnGivenGiftObject->funGiveGiftDetails($senderName,$senderEmail,$senderPhone,$giftCode,$amount,$paypalId); 
        } elseif ( strtolower($gettype)=="getcharity" && $requestMethod == $apiRequestMethod["getcharity"] ) {    
	        $userId =  stripslashes($_REQUEST["userId"]);
	        echo $returnGivenGiftObject->funGetCharityDetails($userId); 
        } elseif ( strtolower($gettype)=="sendmoneyformerchant" && $requestMethod == $apiRequestMethod["sendmoneyformerchant"] ) {    
	        $userId    =  stripslashes($_REQUEST["userId"]);
	        $merchantId =  stripslashes($_REQUEST["merchantId"]);
	        $amount   =  stripslashes($_REQUEST["amount"]);
	        $message =  stripslashes($_REQUEST["message"]);
	        echo $returnGivenGiftObject->funSendMoneyforMerchant($userId,$merchantId,$amount,$message); 
        } elseif ( strtolower($gettype)=="getgiftlist" && $requestMethod == $apiRequestMethod["getgiftlist"] ) {    
	        $userId =  stripslashes($_REQUEST["userId"]);
	        echo $returnGivenGiftObject->funGetGiftListDetails($userId); 
        } elseif ( strtolower($gettype)=="deletegift" && $requestMethod == $apiRequestMethod["deletegift"] ) {    
	        $userId =  stripslashes($_REQUEST["userId"]);
	        $giftId =  stripslashes($_REQUEST["giftId"]);
	        echo $returnGivenGiftObject->funDeleteGiftDetails($userId,$giftId); 
        } elseif ( strtolower($gettype)=="payforgift" && $requestMethod == $apiRequestMethod["payforgift"] ) {    
	        $userId =  stripslashes($_REQUEST["userId"]);
	        $giftId =  stripslashes($_REQUEST["giftId"]);
	        $amount =  stripslashes($_REQUEST["amount"]);
	        $paypalId =  stripslashes($_REQUEST["paypalId"]);
	        echo $returnGivenGiftObject->funPayForGiftDetails($userId,$giftId,$amount,$paypalId); 
        } elseif ( strtolower($gettype)=="transactionshistory" && $requestMethod == $apiRequestMethod["transactionshistory"] ) {   $userid   =  stripslashes($_REQUEST["userId"]);
	        $fromdate =  stripslashes($_REQUEST["fromDate"]);
	        $todate =  stripslashes($_REQUEST["toDate"]);
	       
	        echo $returnGivenGiftObject->funTransactionsHistory($userid,$fromdate,$todate); 
        } elseif ( strtolower($gettype)=="updateprofile" && $requestMethod == $apiRequestMethod["updateprofile"] ) {  
            $userid     =  stripslashes($_REQUEST["userId"]);  
			$name       =  stripslashes($_REQUEST["name"]);
	        $mobile     =  stripslashes($_REQUEST["mobile"]);
	        $address    =  stripslashes($_REQUEST["address"]);
	        $country    =  stripslashes($_REQUEST["country"]);
	        $zip        =  stripslashes($_REQUEST["zip"]);
	        $email      =  stripslashes($_REQUEST["email"]);
	        $password   =  stripslashes($_REQUEST["password"]);
			echo $returnGivenGiftObject->funUpdateProfileDetails($userid,$name,$mobile,$address,$country,$zip,$email,$password);
		} elseif ( strtolower($gettype)=="getreceivedlist" && $requestMethod == $apiRequestMethod["getreceivedlist"] ) {  
            $userid     =  stripslashes($_REQUEST["userId"]);  
			echo $returnGivenGiftObject->funGetReceivedList($userid);
		} elseif ( strtolower($gettype)=="sendmoneyforcharity" && $requestMethod == $apiRequestMethod["sendmoneyforcharity"] ) {  
            $jsonParams  =  stripslashes($_REQUEST["jsonParams"]); 
			echo $returnGivenGiftObject->funSendMoneyForCharity($jsonParams);
		} elseif ( strtolower($gettype)=="creategift" && $requestMethod == $apiRequestMethod["creategift"] ) {  
            $jsonParams  =  stripslashes($_REQUEST["jsonParams"]); 
			echo $returnGivenGiftObject->funCreateGift($jsonParams);
		} elseif ( strtolower($gettype)=="updategift" && $requestMethod == $apiRequestMethod["updategift"] ) {  
            $jsonParams  =  stripslashes($_REQUEST["jsonParams"]); 
			echo $returnGivenGiftObject->funUpdateGift($jsonParams);
		}
        else {
			echo json_encode($errorReqTypeMsg);
		}
	}

?>    