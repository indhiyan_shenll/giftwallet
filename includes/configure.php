<?php
session_start();
global $dbconn;
define("LOCAL_SERVER_NAME","localhost");
define("DEMO_SERVER_NAME","demo.shenll.net");

//PDO connection starts here
try {
    
	if (substr_count($_SERVER['SERVER_NAME'], LOCAL_SERVER_NAME)>0) {
		// Database Informations
		!defined('DB_HOST_NAME') ? define('DB_HOST_NAME',"localhost") : '' ;
		!defined('DATABASE') ? define('DATABASE',"gift_wallet") : '' ;
		!defined('USERNAME') ? define('USERNAME',"root") : '' ;
		!defined('PASSWORD') ? define('PASSWORD',"") : '' ;
	} else if (substr_count($_SERVER['SERVER_NAME'],LIVE_SERVER_NAME)>0) {

		// Database Informations
		!defined('DB_HOST_NAME') ? define('DB_HOST_NAME',"localhost") : '' ;
		!defined('DATABASE') ? define('DATABASE',"gift_wallet") : '' ;
		!defined('USERNAME') ? define('USERNAME',"") : '' ;
		!defined('PASSWORD') ? define('PASSWORD',"") : '' ;
	}

	// Mysql PDO connection
	$dbconn = new PDO("mysql:host=".DB_HOST_NAME.";dbname=".DATABASE,USERNAME,PASSWORD);
	$dbconn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );

} catch (PDOException $e) {
   echo "Connection failed: " . $e->getMessage();
}
